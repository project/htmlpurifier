# HTML Purifier

HTML Purifier is a standards-compliant HTML filter library. HTML Purifier will
not only remove all malicious code (better known as XSS) with a thoroughly
audited, secure yet permissive whitelist, it will also make sure your documents
are standards compliant, something only achievable with a comprehensive knowledge
of W3C's specifications.

HTML Purifier is very tasty when combined with WYSIWYG editors and is more
comprehensive, standards-compliant, permissive and extensive than Drupal's
built-in filtered HTML option, which uses a derivative of kses. You can read
more about it at this comparison page. Want custom fonts, tables, inline
styling, images, and more? Want just a restricted tag set but bullet-proof
standards-compliant output? HTML Purifier is for you!

The HTML Purifier module is licensed under GPL v2 or later, however, the HTML
Purifier library itself is licensed under LGPL v2.1 or later.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/htmlpurifier).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/htmlpurifier).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The HTML Purifier requires 'Text Editor'.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Enable it and configure the filter for each text format in
  Administration - Configuration - Text formats and editors
  (admin/config/content/formats).

- You can use the HTML Purifier Configuration Documentation as reference to
  learn and edit the configuration settings available in YAML format within
  the filter settings form textarea.


## Maintainers

- ezyang - [ezyang](https://www.drupal.org/u/ezyang)
- Lucas Hedding - [heddn](https://www.drupal.org/u/heddn)
- Juan Olalla - [juanolalla](https://www.drupal.org/u/juanolalla)
