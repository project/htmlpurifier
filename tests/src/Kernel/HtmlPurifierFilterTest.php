<?php

namespace Drupal\Tests\htmlpurifier\Kernel;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;
use Drupal\filter\FilterPluginCollection;

/**
 * Tests htmlpurifier filter.
 *
 * @group HtmlPurifier
 */
class HtmlPurifierFilterTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['system', 'filter', 'htmlpurifier'];

  /**
   * The filter.
   *
   * @var \Drupal\htmlpurifier\Plugin\Filter\HtmlPurifierFilter
   */
  protected $filter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $manager = $this->container->get('plugin.manager.filter');
    $bag = new FilterPluginCollection($manager, []);
    $this->filter = $bag->get('htmlpurifier');
  }

  /**
   * Test malicious code is filtered.
   */
  public function testMaliciousCode(): void {
    $input = '<img src="javascript:evil();" onload="evil();" />';
    $expected = '';
    $processed = $this->filter->process($input, 'und')->getProcessedText();
    $this->assertSame($expected, $processed);
  }

  /**
   * Test empty tags are removed.
   */
  public function testRemoveEmpty(): void {
    $input = '<a></a>';
    $expected = '<a></a>';
    $processed = $this->filter->process($input, 'und')->getProcessedText();
    $this->assertSame($expected, $processed);

    $configuration = [
      'AutoFormat' => [
        'RemoveEmpty' => TRUE,
      ],
    ];
    $this->filter->settings['htmlpurifier_configuration'] = Yaml::encode($configuration);

    $expected = '';
    $processed = $this->filter->process($input, 'und')->getProcessedText();
    $this->assertSame($expected, $processed);
  }

  /**
   * Test configuration validation for the filter settings form.
   *
   * @param string $configuration
   *   The HTMLPurifier configuration.
   * @param string[] $expected_errors
   *   The expected errors.
   *
   * @dataProvider providerTestConfigurationValidation
   */
  public function testConfigurationValidation(string $configuration, array $expected_errors): void {
    $element = [
      '#parents' => [
        'filters',
        'htmlpurifier',
        'settings',
        'htmlpurifier_configuration',
      ],
    ];
    $form_state = new FormState();
    $filters['htmlpurifier']['settings']['htmlpurifier_configuration'] = $configuration;
    $form_state->setValue('filters', $filters);

    $this->filter->settingsFormConfigurationValidate($element, $form_state);
    $errors = $form_state->getErrors();
    if (!empty($expected_errors)) {
      $this->assertNotEmpty($errors);
      $this->assertStringContainsString($expected_errors[0], array_values($errors)[0]);
    }
    else {
      $this->assertSame($expected_errors, $errors);
    }
  }

  /**
   * Provides various configuration options.
   */
  public static function providerTestConfigurationValidation(): array {
    $purifier_config = \HTMLPurifier_Config::createDefault();
    $default_configuration = Yaml::encode($purifier_config->getAll());

    return [
      'invalid empty configuration' => [
        '',
        ['HTMLPurifier configuration is not valid'],
      ],
      'default configuration' => [
        $default_configuration,
        [],
      ],
      'undefined directive' => [
        str_replace('RemoveEmpty:', 'FakeDirective:', $default_configuration),
        ['Cannot set undefined directive'],
      ],
      'malformed yaml' => [
        str_replace('RemoveEmpty: false', 'UnexpectedString', $default_configuration),
        ['pars'],
      ],
    ];
  }

}
