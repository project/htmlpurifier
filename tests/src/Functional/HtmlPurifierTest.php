<?php

declare(strict_types=1);

namespace Drupal\Tests\htmlpurifier\Functional;

use Drupal\Component\Serialization\Yaml;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;
use PHPUnit\Framework\Attributes\Group;

/**
 * Test html purifier module.
 */
#[Group('htmlpurifier')]
final class HtmlPurifierTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'filter_test',
    'htmlpurifier',
  ];

  /**
   * Test module configuration and setup.
   */
  public function testConfiguration(): void {
    $purifier_config = \HTMLPurifier_Config::createDefault();
    $default_configuration = Yaml::encode($purifier_config->getAll());
    $full_html_format = FilterFormat::load('full_html');
    assert($full_html_format instanceof FilterFormat);
    $filters = $full_html_format->get('filters');
    $filters['htmlpurifier'] = [
      'status' => TRUE,
      'settings' => [
        'htmlpurifier_configuration' => $default_configuration,
      ],
    ];
    $full_html_format->set('filters', $filters)
      ->save();

    $adminUser = $this->drupalCreateUser([
      'administer filters',
      $full_html_format->getPermissionName(),
    ]);

    $this->drupalLogin($adminUser);
    $this->drupalGet('/admin/config/content/formats/manage/full_html');
    $this->assertSession()->pageTextContains('HTML Purifier Configuration');
  }

}
